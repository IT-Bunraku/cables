from gestalt.crawler.utils import *

################################################################################

class Spider(CrawlEngine):
    def visit(self, link):
        print '\t-> visited:', repr(link.url), 'from:', link.referrer

    def fail(self, link):
        print '\t-> failed:', repr(link.url)

    #***************************************************************************

    def extract(self, doc, schema, match):
        pass # print '\t-> extract: %s | %s | %s' % (link, schema, match)

    def process(self, entry):
        pass


from gestalt.crawler.utils import *

################################################################################

class Spider(CrawlEngine):
    def visit(self, link):
        pass # print '\t-> visited:', repr(link.url), 'from:', link.referrer

    def fail(self, link):
        pass # print '\t-> failed:', repr(link.url)

    #***************************************************************************

    def extract(self, doc, schema, match):
        print '\t-> extract: %s | %s' % (doc.link.url, match)

    def process(self, entry):
        pass


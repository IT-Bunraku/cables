#!/usr/bin/env python

from slackclient import SlackClient

from gestalt.cmd.management import *

from uchikoma.opendata.shortcuts import *

################################################################################

class Command(Mongo_Sync):
    help = 'Synchronize Cables.'

    def add_options(self, parser):
        pass

    #***************************************************************************

    def prepare(self, *args, **options):
        self.connect('mongodb://mist:optimum@ds039135.mlab.com:39135/vortex')

    def terminate(self, *args, **options):
        pass

    #***************************************************************************

    def loop(self, *args, **options):
        print "*) Scanning SPARQL endpoints :"

        for ep in SparqlEndpoint.objects.all():
            yield self.process_sparql, [ep], options

        print "*) Scanning CKAN hubs :"

        for hub in CKAN_DataHub.objects.all():
            yield self.process_ckan, [hub], options

        try:
            print "*) Updating Data Hubs :"

            hubs = requests.get('http://instances.ckan.org/config/instances.json').json()
        except:
            print "#) Error while scanning for new CKAN hubs."

        for entry in hubs:
            yield self.process_datahub, [entry], options

    #***************************************************************************

    def process_sparql(self, ep, **options):
        print "\t-> %(link)s ..." % ep.__dict__

        #scan_sparql.delay(alias=ep.alias)

    def process_ckan(self, hub, **options):
        print "\t-> %(realm)s ..." % hub.__dict__

        #scan_ckan.delay(alias=hub.alias)

    def process_datahub(self, entry, **options):
        hub,st = CKAN_DataHub.objects.get_or_create(alias=entry['id'])

        hub.realm   = entry.get('url-api', entry['url'])
        hub.version = 3

        hub.save()

        if st:
            scan_ckan.delay(alias=hub.alias)

        print "\t-> Adding CKAN Hub '%(title)s' at : %(url)s" % entry


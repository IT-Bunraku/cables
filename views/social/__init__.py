#-*- coding: utf-8 -*-

from uchikoma.cables.shortcuts import *

from uchikoma.connector.schemas import *

################################################################################

@Reactor.router.register_route('social', r'^$', strategy='login')
class Homepage(Reactor.ui.Dashboard):
    template_name = "social/views/homepage.html"

    def enrich(self, request, **context):
        context['primitives'] = enumerate_schemas(owner_id=request.user.pk)

        return context

    def populate(self, request, **context):
        for label,icon,stats in context['primitives']:
            yield Reactor.ui.Block('counter', label.lower(),
                title=label, icon=icon,
                size=dict(md=3, xs=6),
            value=stats)

#*******************************************************************************

# @Reactor.router.register_route('connect', r'^$', strategy='login')
@render_to('rest/identity/aggregate/timeline.html')
def chronology(request):
    facet = {
        'owner_id':     request.user.pk,
        'ontology__in': [x for x in [
            [onto.ns_alias] + onto.ns_types
            for onto in TIMELINE_TYPEs
        ] if x not in ('tweet','link')],
    }

    graph = Post.objects(**facet).item_frequencies('source')

    if 'network' in request.GET:
        facet['source'] = request.GET.get('network')

    return context(
        profile=request.user,
        networks=[
            (k, str(v))
            for k,v in graph.iteritems()
        ],
        listing=Post.objects(**facet).order_by('-when'),
    )

################################################################################



from .utils import *

################################################################################

@Reactor.orm.register_model('rss_feed')
class RssFeed(models.Model):
    owner      = models.ForeignKey(Identity, related_name='rss_feeds')
    link       = models.CharField(max_length=512)

    title      = models.CharField(max_length=255, blank=True)
    summary    = models.TextField(blank=True)
    when       = models.DateTimeField(blank=True, auto_now_add=True)
    publish_at = models.DateTimeField(blank=True, auto_now_add=True)

    @property
    def proxy(self):
        if not hasattr(self, '_feed'):
            setattr(self, '_feed', None)

        if self._feed is None:
            import feedparser

            self._feed = feedparser.parse(self.link)

        return self._feed

    def refresh(self):
        pass

#*******************************************************************************

from wordpress_xmlrpc         import Client        as WordPressClient
from wordpress_xmlrpc         import WordPressPost
from wordpress_xmlrpc.methods import posts         as WP_Posts

@Reactor.orm.register_model('wp_site')
class WordpressSite(models.Model):
    owner      = models.ForeignKey(Identity, related_name='wordpress')
    link       = models.CharField(max_length=512)

    username   = models.CharField(max_length=255)
    password   = models.CharField(max_length=255)

    title      = models.CharField(max_length=255, blank=True)
    summary    = models.TextField(blank=True)

    @property
    def proxy(self):
        if not hasattr(self, '_rpc'):
            setattr(self, '_rpc', None)

        if self._feed is None:
            self._rpc = WordPressClient(self.link, self.username, self.password)

        return self._rpc

    def get_posts(self, **lookup):
        if len(lookup)==0:
            lookup.update({'post_type': 'post'})

        return self.proxy.call(WP_Posts.GetPosts(lookup))

    def refresh(self):
        pass


wamp = require "cupholder"

module.exports = (robot) ->
    conn = new wamp.Client "ws://hub.proto.uchikoma.xyz/junction", debug=true

    conn.on "error", (e) -> console.error e
    conn.on "open", () ->
        console.log "session opened"
        promise = conn.authenticate "username", "password"
        promise.then (result) ->
            console.log "WAMP authenticated"
            conn.subscribe "http://example.com/feed"

            robot.enter (res) ->
                conn.publish "http://example.com/comments", "hi", excludeMe=false
                res.send res.random enterReplies

            robot.leave (res) ->
                res.send res.random leaveReplies

            robot.hear /badger/i, (res) ->
                # your code here

            robot.respond /open the (.*) doors/i, (res) ->
                doorType = res.match[1]

                if doorType is "pod bay"
                    res.reply "I'm afraid I can't let you do that."
                else
                    res.reply "Opening #{doorType} doors"


        , (error) ->
            console.error error[1]

    conn.on "close", () ->
        console.log "session closed"

    conn.on "event", (uri, event) ->
        console.log uri, event


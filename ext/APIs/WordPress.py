#!/usr/bin/python

import os, sys, yaml, json

DATA_FILE  = lambda *x: os.path.join(os.path.dirname(__file__), 'data',  *x)
MEDIA_FILE = lambda *x: os.path.join(os.path.dirname(__file__), 'media', *x)

################################################################################

from wordpress_xmlrpc import Client, WordPressPost

from wordpress_xmlrpc.methods import posts

#*******************************************************************************

def load_wordpress(payload, endpoint, pseudo, passwd):
    client = Client(endpoint, pseudo, passwd)

    for ontology in ['board']:
        for entry in client.call(posts.GetPosts({'post_type': ontology})):
            client.call(posts.DeletePost(entry.id))

    for board in :
        resp = WordPressPost()

        print (board)

        resp.post_type = 'board'
        resp.title = '%s %s' % (
            board['family'].capitalize(),
            board['model'].capitalize(),
        )
        if 'summary' in board:
            resp.excerpt = board['summary']

        resp.content = open(MEDIA_FILE('%(family)s/content/%(model)s.txt' % board)).read()
        resp.id = client.call(posts.NewPost(resp))

        # whoops, I forgot to publish it!
        resp.post_status = 'publish'
        client.call(posts.EditPost(resp.id, resp))

################################################################################

#if __name__=='__main__':
#    payload = yaml.load(open(DATA_FILE('board.yaml')).read())

#    load_wordpress(payload, 'http://elmoudenindustriel.com/xmlrpc.php', 'admin', 'adminlws')


from django.contrib import admin

from import_export.admin import ImportExportModelAdmin, ImportExportActionModelAdmin

from .models import *

################################################################################

class RssFeedAdmin(ImportExportActionModelAdmin):
    list_display  = ('owner', 'link', 'title', 'when', 'publish_at')
    list_filter   = ('owner__username', 'when', 'publish_at')
    list_editable = ('title',)

admin.site.register(RssFeed, RssFeedAdmin)

################################################################################

class WordpressAdmin(ImportExportActionModelAdmin):
    list_display  = ('owner', 'link', 'username', 'title', 'summary')
    list_filter   = ('owner__username', 'username')
    list_editable = ('title', 'summary')

admin.site.register(WordpressSite, WordpressAdmin)


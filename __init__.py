subdomains = {
    'cables':   dict(icon='rss',      title="Cables"),
    'mail':     dict(icon='envelope', title="MailBox"),
    'news':     dict(icon='rss',      title="Newswire"),
    'social':   dict(icon='users',    title="Social Networking"),
}


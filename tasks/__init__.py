from uchikoma.cables.utils import *

from uchikoma.cables.models  import *
from uchikoma.cables.schemas import *
from uchikoma.cables.graph   import *

#*******************************************************************************

from uchikoma import settings

################################################################################

@Reactor.rq.register_task(queue='perso')
def refresh_rssfeed(rss_id, **response):
    rss = RssFeed.objects.get(pk=rss_id)

    if rss.proxy:
        for entry in rss.proxy.entries:
            upsert(StoryPost, *StoryPost.from_feedparser(rss.owner.pk, entry))

        rss.title      = rss.proxy.feed.title
        rss.summary    = rss.proxy.feed.description

        if hasattr(rss.proxy.feed, 'updated'):
            rss.publish_at = dt_parser(rss.proxy.feed.updated)
        else:
            rss.publish_at = timezone.now()

        rss.save()

        print "*) Processed RSS Feed : %(link)s" % rss.__dict__
    else:
        print "*) Skipped RSS Feed : %(link)s" % rss.__dict__

################################################################################

@Reactor.rq.register_task(queue='perso')
def refresh_wordpress(wp_id, **response):
    site = WordpressSite.objects.get(pk=wp_id)

    for entry in site.get_posts():
        upsert(ArticlePost, *ArticlePost.from_wordpress(site.pk, entry))

        rss.title      = rss.proxy.feed.title
        rss.summary    = rss.proxy.feed.description

        site.save()

        print "*) Processed WP site : %(link)s" % rss.__dict__
    else:
        print "*) Skipped WP site : %(link)s" % rss.__dict__

